output "example_for" {
  value = {for entry in local.bucket_objects : "${entry.bucket}.${entry.object}" => entry}
}