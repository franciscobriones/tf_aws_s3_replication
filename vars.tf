variable "access_key" {
  default = ""
}

variable "secret_key" {
  default = ""
}

variable "region" {
  default = "us-east-1"
}


variable "stage" {
  type        = string
  description = "Deployment stage/environment name"
  default     = "development"
}

variable "organizationalUnit" {
  type        = string
  description = "Comunity or Gobernance Name"
  default     = "gobcomp-rpacontrol"
}

variable "description" {
  type        = string
  description = "The description of this S3 bucket"
  default     = "S3 Bucket to hold your Data"
}

variable "managedBy" {
  description = "Managed By automation tool name"
  default     = "Victor Tapia"
}

variable "tags" {
  description = "A map of tags to add to all resources"
  default     = {}
}

variable "public" {
  description = "Allow public read access to bucket"
  default     = "private"
}

variable "force_destroy" {
  description = "Delete all objects in bucket on destroy"
  default     = true
}

variable "versioned" {
  description = "Version the bucket"
  default     = true
}

variable "lifecycle_enabled" {
  description = "Is Lifecycle enabled?"
  default     = false
}

variable "iam_role_name" {
  type        = string
  description = "IAM Role name for replication"
  default     = "demo-bucket-replication"
}

variable "default_s3_names_for_apps" {
  description = "The default content of the s3 bucket upon creation of the bucket"
  type        = set(string)
  default     = ["inotify", "warflow", "analistacondell", "contratosforward", "portacondell"]
}

variable "default_s3_object_names_for_apps" {
  description = "The default content of the s3 bucket upon creation of the bucket"
  type        = set(string)
  default     = ["inbound", "outbound", "logs"]
}

variable "default_s3_content_for_replica" {
  description = "The default content of the s3 bucket upon creation of the bucket"
  type        = set(string)
  default = [
    "inotify/inbound",
    "inotify/outbound",
    "warflow/inbound",
    "warflow/outbound",
    "analistacondell/inbound",
    "analistacondell/outbound",
    "contratosforward/inbound",
    "contratosforward/outbound",
    "portacondell/inbound",
    "portacondell/outbound"
  ]
}

locals {
  bucket_objects = distinct(flatten([
    for bucket in var.default_s3_names_for_apps : [
      for object in var.default_s3_object_names_for_apps : {
        bucket = bucket
        object = object
      }
    ]
  ]))
}
