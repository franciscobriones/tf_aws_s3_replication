resource "aws_iam_role" "iam_role_replication" {
  name = "tf_iam_role_replication-${random_uuid.uuid.result}"
  assume_role_policy = data.aws_iam_policy_document.assume_role.json
}