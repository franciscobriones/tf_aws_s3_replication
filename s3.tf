######################################
############ AWS S3 ##################
######################################
resource "aws_s3_bucket" "s3_bucket_for_replica" {
  bucket        = "${var.stage}-${var.organizationalUnit}-bucket-test-replica"
  acl           = var.public
  force_destroy = var.force_destroy
  server_side_encryption_configuration {
    rule {
      apply_server_side_encryption_by_default {
        sse_algorithm = "AES256"
      }
    }
  }
  tags = merge(
    local.global_tags,
    {
      Name = "${var.stage}-${var.organizationalUnit}-bucket-test-replica"
    },
  )
}

resource "aws_s3_bucket" "s3_bucket_for_apps" {
  for_each      = var.default_s3_names_for_apps
  bucket        = "${var.stage}-${var.organizationalUnit}-${each.value}"
  acl           = var.public
  force_destroy = var.force_destroy

  server_side_encryption_configuration {
    rule {
      apply_server_side_encryption_by_default {
        sse_algorithm = "AES256"
      }
    }
  }
  tags = merge(
    local.global_tags,
    {
      Name = "${var.stage}-${var.organizationalUnit}-${each.value}"
    },
  )
}

resource "aws_s3_bucket_versioning" "s3_bucket_versioning_for_replica" {
  bucket = aws_s3_bucket.s3_bucket_for_replica.id
  versioning_configuration {
    status = "Enabled"
  }
}

resource "aws_s3_bucket_versioning" "s3_bucket_versioning_for_apps" {
  for_each = var.default_s3_names_for_apps
  bucket   = aws_s3_bucket.s3_bucket_for_apps[each.value].id
  versioning_configuration {
    status = "Enabled"
  }
}

resource "aws_s3_object" "default_S3_init_for_replica" {
  for_each = var.default_s3_content_for_replica
  bucket   = aws_s3_bucket.s3_bucket_for_replica.id
  key      = "${each.value}/"
}

resource "aws_s3_object" "default_S3_init_for_apps" {
  for_each = { for entry in local.bucket_objects : "${entry.bucket}.${entry.object}" => entry }
  bucket   = aws_s3_bucket.s3_bucket_for_apps[each.value.bucket].id
  key      = "${each.value.object}/"
}

# resource "aws_s3_bucket_object" "update_file_for_test_replica" {
#   bucket = aws_s3_bucket.s3_bucket.bucket
#   key    = "/contratosforward/inbound/img.png"
#   acl    = "private" # or can be "public-read"
#   source = "./files/img.png"
#   etag   = filemd5("files/img.png")
#   depends_on = [
#     aws_s3_object.default_S3_init
#   ]
# }

resource "aws_s3_bucket_replication_configuration" "s3_bucket_replication_configuration_app_portacondell" {
  depends_on = [
    aws_s3_bucket_versioning.s3_bucket_versioning_for_replica
  ]
  role   = aws_iam_role.iam_role_replication.arn
  bucket = aws_s3_bucket.s3_bucket_for_replica.id

  rule {
    id = "s3-replication-config-portacondell"
    filter {
      prefix = "portacondell/inbound/" # subdirectorios de destino
    }

    delete_marker_replication {
      status = "Enabled"
    }
    status = "Enabled"

    destination {
      bucket        = aws_s3_bucket.s3_bucket_for_apps["portacondell"].arn
      storage_class = "STANDARD"
    }
  }
}
