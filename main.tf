provider "aws" {
  region     = var.region
  access_key = var.access_key
  secret_key = var.secret_key
}

data "aws_availability_zones" "azs" {
  state = "available"
}

locals {
  global_tags = merge(
    {
      "Environment"        = var.stage
      "OrganizationalUnit" = var.organizationalUnit
      "Description"        = var.description
      "ManagedBy"          = var.managedBy
    },
    var.tags,
  )
}
