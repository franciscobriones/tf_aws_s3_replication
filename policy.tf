resource "aws_iam_policy" "iam_policy_replication" {
  name   = "tf-iam-role-policy-replication-${random_uuid.uuid.result}"
  policy = data.aws_iam_policy_document.iam_policy_document_replication.json
}

resource "aws_iam_role_policy_attachment" "iam_role_policy_attachment_replication" {
  role       = aws_iam_role.iam_role_replication.name
  policy_arn = aws_iam_policy.iam_policy_replication.arn
}

# resource "aws_iam_role_policy_attachment" "replication" {
#   role       = aws_iam_role.replication.name
#  #policy_arn = "arn:aws:iam::777790172967:policy/s3_policy_for_replication_configuration"
#  # policy_arn = aws_iam_policy.replication.arn
#}

