# AWS S3 Replication

![image.png](./images/image.png)
## Bucket-to-Bucket Replication
#### The simplest solution is creating a destination bucket with the appropriate replication and permission configuration. After setting up the replication policy between the two buckets, any object uploaded to the source bucket is automatically copied to the destination bucket.

## Important
#### Replicating all objects in a bucket may pose a security problem. Instead, we can target a subset of objects to copy by specifying a prefix.